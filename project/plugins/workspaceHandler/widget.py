from PySide2 import QtWidgets, QtGui, QtCore
from ui.structure_dock import StructureDock
from PySide2.QtWidgets import QLabel, QMainWindow, QApplication
from PySide2.QtGui import QBrush, QPen
from PySide2.QtCore import Qt
import os

import random
import json


class graphicsScene(QtWidgets.QGraphicsScene):
    def __init__ (self, parent):
        super(graphicsScene, self).__init__(parent)
        self.gp = parent
        self.brojac = 0
        #self.gp.main_window.document = "PERA"
        

    def mousePressEvent(self, event):
        super(graphicsScene, self).mousePressEvent(event)
        self.selected = self.selectedItems()
        
        for item in self.selected:
            self.broj_itema = item.text[0]
            self.gp.main_window.switch(self.broj_itema[-1])
            #print(self.broj_itema[-1])


class WorkspaceHandler(QtWidgets.QGraphicsView):
    def __init__(self, parent):
        super().__init__(parent)
        self.main_window = parent
        
  
        self.input_dialog = QtWidgets.QInputDialog()
        self.scene = graphicsScene(self) #Dodavanje scene
        self.view = QtWidgets.QGraphicsView(self.scene, self)
        self.view.setGeometry(0, 0, 640, 440)   

         
        

        #Dve liste koje rade kao stek
        self.lista = ["Workspace 1"]
        self.lista_dodati = []
        self.text_lista = []
        self.lista_rektova = []
        self.indeks = None
        self.ime = None
        self.range = 20
        self.brojac = 0
        #Toolbar and actions
        self.tool_bar = QtWidgets.QToolBar("Naslov", self)

        insert = QtWidgets.QAction(QtGui.QIcon("resources/icons/plus.png",),"&Dodaj workspace", self,
                shortcut=QtGui.QKeySequence.Undo,
                statusTip="Undo", triggered=self.insert)
       
        
        self.tool_bar.addAction(insert)
      
       
        self._layout = QtWidgets.QVBoxLayout()
        self._layout.addWidget(self.tool_bar)
        self._layout.addWidget(self.view)
        self.setLayout(self._layout)
        self.path = None
        self.populate()
        self.populate2()
        print(self.lista)

       

        
   

    def obrisi_item(self):
            self.scene.clear()

    def elementi_liste_dodato(self):
                for i in self.lista_dodati:
                        print("Dodato " + i)

    def elementi_liste(self):
            if self.ime is not None:
                for i in self.lista:
                        print(i)


    def populate(self):
            print("Prikazani su radni prostori")
            for i in self.lista:
                if i not in self.lista_dodati:
                        #self.text = i #Napravimo text
                        self.text_lista.append(i)
                        self.text_item = self.scene.addText(self.text_lista[self.brojac]) # Dodamo text na scenu koju zatim dodamo na item

                        self.rect_item = RectItem(0, 0, 100, 20, self.text_item)  #rekt itenm
                        self.text_item.setX(-200) # Podesimo kordinate itema
                        self.text_item.setY(self.range) # Podesimo kordinate itema
                        self.text_item.setFlag(QtWidgets.QGraphicsItem.ItemIsSelectable, True)
                        self.rect_item.setFlag(QtWidgets.QGraphicsItem.ItemIsMovable, True)
                        self.rect_item.setFlag(QtWidgets.QGraphicsItem.ItemIsSelectable, True)

                        self.rect_item.text.append(i)
                        self.lista_dodati.append(i) # Dodamo item na listu
                        self.range += 20
                        self.brojac += 1
                        self.lista_rektova.append(self.rect_item)
                
                        

  

    def populate2(self):
        self.testButton = GraphicsButton()
        self.scene.addItem(self.testButton)


    def insert(self):
        self.main_window.add_wp()
        
        

    def rename(self):
        self.kliknuti = self.main_window.tree.model.get_item(self.indeks) # Uzimamo item od selektovanog indexa 
        self.input = self.input_dialog.getText(self, 'Novo ime', 'Novo ime: ')  
        self.kliknuti.name = self.input[0]

    def save(self):
        self.kliknuti = self.main_window.tree.model.get_item(self.indeks) # Uzimamo item od selektovanog indexa
        self.string = self.kliknuti.to_dict() # Serijalizujemo kliknuti element
        out_file = open("meta/meta.json", "w")  # Otvaramo json dokument za pisanje
        json.dump(self.string, out_file, indent = 6)  #Zapisujemo serijalizovan dokument
        out_file.close()  # Zatvaramo json

    def delete_command(self): # Brisanje na desni klik selektovanog elementa
        index = self.indeks
        
        self.kliknuti = self.main_window.tree.model.get_item(index) 
        self.main_window.tree.model.beginRemoveRows(index.parent(), index.row(), index.row()) # Pocetak brisanja 
        self.main_window.tree.model.removeRow(index.row(), index.parent()) # Brisanje iz modela
        self.kliknuti.removeChildren(0, self.main_window.tree.model.rowCount(index)) # Brisanje dece iz item-a
        self.main_window.tree.model.endRemoveRows() # Zavrsetak brisanja redova
        #self.delete_lista(self.kliknuti.name) # Brisanje kliknutog elementa iz obe liste widget-a, radi uklanjanja elementa iz prikaza
        print(self.kliknuti.name)
    
    def kita(self):
        return self.indeks
    

class RectItem(QtWidgets.QGraphicsRectItem):
    def __init__(self, x, y, w, h, parent):
        super(RectItem, self).__init__(x, y, w, h, parent)

        self.setFlags(self.ItemIsSelectable|self.ItemIsMovable)
        #self.brojac = 1
        self.text = []


class GraphicsButton(QtWidgets.QGraphicsPolygonItem):
    def __init__(self, parent=None):
        super().__init__(parent)
        self.main_window = parent

        self.textItem = QtWidgets.QGraphicsSimpleTextItem('Rad sa radnim prostorima', self)
        self.rect = self.textItem.boundingRect()
        self.rect.moveCenter(self.boundingRect().center())
        self.textItem.setPos(self.rect.topLeft())
        


