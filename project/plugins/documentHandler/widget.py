from PySide2 import QtWidgets, QtGui, QtCore
from ui.structure_dock import StructureDock
from PySide2.QtWidgets import QLabel, QMainWindow, QApplication
from PySide2.QtGui import QBrush, QPen
from PySide2.QtCore import Qt
import os
import random
import json


class DocumentHandler(QtWidgets.QGraphicsView):
    def __init__(self, parent):
        super().__init__(parent)
        self.main_window = parent

        self.input_dialog = QtWidgets.QInputDialog()
        self.scene = QtWidgets.QGraphicsScene(0, 0, 200, 200, self) #Dodavanje scene
        self.view = QtWidgets.QGraphicsView(self.scene, self)
        self.view.setGeometry(0, 0, 640, 440)

        #Dve liste koje rade kao stek
        self.lista1 = []
        self.lista_dodati1 = []

        self.indeks = None
        self.ime = None
        self.range = -30

        #Toolbar and actions
        self.tool_bar = QtWidgets.QToolBar("Naslov", self)

        insert = QtWidgets.QAction(QtGui.QIcon("resources/icons/plus.png",),"&Insert", self,
                shortcut=QtGui.QKeySequence.Undo,
                statusTip="Undo", triggered=self.insert)
        delete = QtWidgets.QAction(QtGui.QIcon("resources/icons/delete.png"),"&Delete", self,
                shortcut=QtGui.QKeySequence.Undo,
                statusTip="Undo", triggered=self.delete_command)
        rename = QtWidgets.QAction(QtGui.QIcon("resources/icons/eraser.png"),"&Ocisti", self,
                shortcut=QtGui.QKeySequence.Undo,
                statusTip="Save", triggered=self.obrisi_item)
        undo = QtWidgets.QAction(QtGui.QIcon("resources/icons/save.png"),"&Save", self,
                shortcut=QtGui.QKeySequence.Undo,
                statusTip="Undo", triggered=self.save)
        pedo = QtWidgets.QAction(QtGui.QIcon("resources/icons/edit.png"),"&Rename", self,
                shortcut=QtGui.QKeySequence.Undo,
                statusTip="Rename", triggered=self.rename)
        
        self.tool_bar.addAction(insert)
        self.tool_bar.addAction(delete)
        self.tool_bar.addAction(rename)
        self.tool_bar.addAction(undo)
        self.tool_bar.addAction(pedo)
       
        self._layout = QtWidgets.QVBoxLayout()
        self._layout.addWidget(self.tool_bar)
        self._layout.addWidget(self.view)
        self.setLayout(self._layout)
        self.path = None
        

    def obrisi_item(self):
            self.scene.clear()

    def elementi_liste_dodato(self):
                for i in self.lista_dodati1:
                        print("Dodato " + i)

    def elementi_liste(self):
            if self.ime is not None:
                for i in self.lista1:
                        print(i)
                        
    '''
    def populateee(self):
        for i in self.ime.elements:
            self.text = i.name
            self.text_item = self.scene.addText(self.text) # Dodamo text na scenu koju zatim dodamo na item
            self.rect_item = QtWidgets.QGraphicsRectItem(QtCore.QRectF(0, 0, 200, 40), self.text_item)  #rekt itenm
            self.text_item.setX(-150) # Podesimo kordinate itema
            self.text_item.setY(self.range) # Podesimo kordinate itema
            self.rect_item.setFlag(QtWidgets.QGraphicsItem.ItemIsMovable, True)
            self.lista_dodati.append(i) # Dodamo item na listu
            self.range += 20
            self.text_item.setFont(QtGui.QFont('Times', 20))
    '''
    
    def populate(self):
            print("Prikazan JE DOKUMENT")
            for i in self.lista1:
                if i not in self.lista_dodati1:
                        self.text = i #Napravimo text
                        self.text_item = self.scene.addText(self.text) # Dodamo text na scenu koju zatim dodamo na item
                        self.rect_item = QtWidgets.QGraphicsRectItem(QtCore.QRectF(0, 0, 200, 40), self.text_item)  #rekt itenm
                        self.text_item.setX(-250) # Podesimo kordinate itema
                        self.text_item.setY(self.range) # Podesimo kordinate itema
                        self.rect_item.setFlag(QtWidgets.QGraphicsItem.ItemIsMovable, True)
                        self.lista_dodati1.append(i) # Dodamo item na listu
                        self.range += 40
                        self.text_item.setFont(QtGui.QFont('Times', 15))
                
    

    def populate2(self):
        self.testButton = GraphicsButton()
        self.scene.addItem(self.testButton)

    def clear(self):
        self.scene.clear()
        self.lista1.clear()
        self.lista_dodati1.clear()
        self.range = 20

    def insert(self):
        self.main_window.tree.model.beginInsertRows(self.indeks, self.indeks.row(), self.indeks.column())
        self.dete_kliknutog = self.main_window.tree.model.create_child(self.ime.child_title, self.ime, self.indeks) #Kreiramo dete kliknutog indexa
        self.ime.appendChild(self.dete_kliknutog)
        self.main_window.tree.model.endInsertRows()
        self.lista1.append(self.dete_kliknutog.name)
        self.populate()
        

    def rename(self):
        self.kliknuti = self.main_window.tree.model.get_item(self.indeks) # Uzimamo item od selektovanog indexa 
        self.input = self.input_dialog.getText(self, 'Novo ime', 'Novo ime: ')  
        self.kliknuti.name = self.input[0]

    def save(self):
        self.kliknuti = self.main_window.tree.model.get_item(self.indeks) # Uzimamo item od selektovanog indexa
        self.string = self.kliknuti.to_dict() # Serijalizujemo kliknuti element
        out_file = open("meta/meta.json", "w")  # Otvaramo json dokument za pisanje
        json.dump(self.string, out_file, indent = 6)  #Zapisujemo serijalizovan dokument
        out_file.close()  # Zatvaramo json

    def delete_command(self): # Brisanje na desni klik selektovanog elementa
        index = self.indeks
        
        self.kliknuti = self.main_window.tree.model.get_item(index) 
        self.main_window.tree.model.beginRemoveRows(index.parent(), index.row(), index.row()) # Pocetak brisanja 
        self.main_window.tree.model.removeRow(index.row(), index.parent()) # Brisanje iz modela
        self.kliknuti.removeChildren(0, self.main_window.tree.model.rowCount(index)) # Brisanje dece iz item-a
        self.main_window.tree.model.endRemoveRows() # Zavrsetak brisanja redova
        #self.delete_lista(self.kliknuti.name) # Brisanje kliknutog elementa iz obe liste widget-a, radi uklanjanja elementa iz prikaza
        print(self.kliknuti.name)



class GraphicsButton(QtWidgets.QGraphicsPolygonItem):
    def __init__(self, parent=None):
        super().__init__(parent)
        
        self.textItem = QtWidgets.QGraphicsSimpleTextItem('DOCUMENT HANDLER', self)
        self.textItem.setFont(QtGui.QFont('Times', 25))
        
        self.rect = self.textItem.boundingRect()
        self.textItem.setX(-100) # Podesimo kordinate itema
        self.textItem.setY(-150) # Podesimo kordinate itema

