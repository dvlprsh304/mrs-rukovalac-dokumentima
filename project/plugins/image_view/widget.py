from PySide2 import QtWidgets, QtGui, QtCore
from ui.structure_dock import StructureDock
from PySide2.QtWidgets import QLabel, QMainWindow, QApplication
import os


#image_path = r"C:\Users\Marko\Desktop\project\plugins\image_view\kod2.png"

#from ui.main_window import MainWindow

class ImageView(QtWidgets.QWidget):
    def __init__(self, parent):
        super().__init__(parent)
        self.main_window = parent
        
       
        self.image = QtGui.QImage()
        self._layout = QtWidgets.QVBoxLayout()
        self.lbl = QLabel(self)

        self.path = None

        
        #Kreiranje ToolBar-a
        self.tool_bar = QtWidgets.QToolBar("Naslov", self)
        #AllActions
        openAction = QtWidgets.QAction(QtGui.QIcon("resources/icons/openfile.png"),"&Open", self,
                shortcut=QtGui.QKeySequence.Undo,
                statusTip="Undo", triggered=self.file_open)
                
        rotateAction = QtWidgets.QAction(QtGui.QIcon("resources/icons/redo.png"),"&Rotate", self,
                shortcut=QtGui.QKeySequence.Redo,
                statusTip="Redo", triggered=self.rotirati)
        
        
        #Dodavanje akcija
        self.tool_bar.addAction(openAction)
        self.tool_bar.addAction(rotateAction)
        

        self._layout.addWidget(self.tool_bar)
        self._layout.addWidget(self.lbl)
        self.setLayout(self._layout)

    def rotirati(self):
        self.transform = QtGui.QTransform().rotate(90)
        self.image = self.image.transformed(self.transform)
        self.update()

    def zumirati(self):
        # scale
        self.transform = QtGui.QTransform().scale(2, 2)
        self.image = self.image.transformed(self.transform)
        self.update()



    def set_image(self, file_name):
        self.image.load(file_name)

    def paintEvent(self, event):
        if self.image:
            self.pixmap = QtGui.QPixmap(self.image)
            self.lbl.setPixmap(self.pixmap.scaled(self.lbl.size() , QtCore.Qt.KeepAspectRatio , QtCore.Qt.SmoothTransformation))

    def file_open(self):
        file_name, filter_ = QtWidgets.QFileDialog.getOpenFileName(
            self,
            caption="Open asd",
            dir=os.path.expanduser("~/Pictures/ron.sh"),
            
        )
        self.set_image(file_name)
        self.update()
