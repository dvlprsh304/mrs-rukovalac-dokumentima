from plugin_framework.extension import Extension
from .widget import ContentHandler
from ui.main_window import MainWindow

class Plugin(Extension):
    
    def __init__(self, specification, iface):
        """
        :param iface: main_window aplikacije
        """
        super().__init__(specification, iface)
        self.widget = ContentHandler(iface)
        print("INIT TEST")

    # FIXME: implementacija apstraktnih metoda
    def activate(self):
        print("Activated")
        self.iface.add_widget(self.widget)
        

    def deactivate(self):
        print("Deactivated")
        self.iface.remove_widget(self.widget)