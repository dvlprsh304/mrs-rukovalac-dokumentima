from PySide2 import QtWidgets, QtGui, QtCore
from ui.structure_dock import StructureDock
from PySide2.QtWidgets import QLabel, QMainWindow, QApplication
import os
from ui.tool_bar import ToolBar
import csv



class TablegeView(QtWidgets.QWidget):
   def __init__(self, parent):
      super().__init__(parent)
      self.main_window = parent
      self.path = None
      

      
      self.model = QtGui.QStandardItemModel(self)
      self.table = QtWidgets.QTableView(self)
      self.table.setModel(self.model)
      
      
      #ToolBar
      self.tool_bar = QtWidgets.QToolBar("Naslov", self)

       
      saveAction = QtWidgets.QAction(QtGui.QIcon("resources/icons/save.png"),"&Save", self,
            shortcut=QtGui.QKeySequence.Save,
            statusTip="Save", triggered=self.file_save)

      
     
         
      self.tool_bar.addAction(saveAction)
      
      self._layout = QtWidgets.QVBoxLayout()
      self._layout.addWidget(self.tool_bar)
      self._layout.addWidget(self.table)
     
      self.setLayout(self._layout)

   

   def file_open(self):
      pass

   def file_save(self):
        if self.path is None:
            # Ako nemamo path, moramo da koristimo Save As.
            return self.file_saveas()
        
        self._save_to_path(self.path)

   def file_saveas(self):
        path, _ = QtWidgets.QFileDialog.getSaveFileName(self, "Save file", "", "Tables (*.csv)")

        if not path:
            #If dialog is canceled, will return ''
            return

        self._save_to_path(path)

   def _save_to_path(self, path):
      with open(path, "w") as fileOutput:
         self.writer = csv.writer(fileOutput)
         for self.rowNumber in range(self.model.rowCount()):
            self.fields = [
               self.model.data(
                     self.model.index(self.rowNumber, self.columnNumber),
                     QtCore.Qt.DisplayRole
               )
               for self.columnNumber in range(self.model.columnCount())
            ]
            self.writer.writerow(self.fields)
      

   