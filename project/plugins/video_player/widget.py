from PySide2 import QtWidgets, QtGui, QtCore
from PySide2.QtCore import QObject
from PySide2.QtMultimedia import QMediaPlayer, QMediaPlaylist, QMediaContent
from PySide2.QtMultimediaWidgets import QVideoWidget
from ui.structure_dock import StructureDock
from PySide2.QtWidgets import QLabel, QMainWindow, QApplication, QFileDialog, QPushButton, QSizePolicy, QSlider, QStyle 
import os
import sys


class VideoPlayer(QtWidgets.QWidget):
    def __init__(self, parent):
        super().__init__(parent)
        self.main_window = parent

        self.layout = QtWidgets.QVBoxLayout()

        self.setWindowTitle("PyQt Video Player Widget Example - pythonprogramminglanguage.com") 
        
        self.mediaPlayer = QMediaPlayer(None, QMediaPlayer.VideoSurface)
        self.videoWidget = QVideoWidget()

        self.playButton = QPushButton()
        self.playButton.setEnabled(False)
        self.playButton.setIcon(self.style().standardIcon(QStyle.SP_MediaPlay))
        self.playButton.clicked.connect(self.play)

        self.positionSlider = QSlider(QtCore.Qt.Horizontal)
        self.positionSlider.setRange(0, 0)
        self.positionSlider.sliderMoved.connect(self.setPosition)

        self.errorLabel = QLabel()
        self.errorLabel.setSizePolicy(QSizePolicy.Preferred,
                QSizePolicy.Maximum)

         
        #TOOLBAR

        self.tool_bar = QtWidgets.QToolBar("Naslov", self)

        openAction = QtWidgets.QAction(QtGui.QIcon("resources/icons/undo.png"),"&Open", self,
                shortcut=QtGui.QKeySequence.Undo,
                statusTip="Undo", triggered=self.openFile)
                
        exitAction = QtWidgets.QAction(QtGui.QIcon("resources/icons/redo.png"),"&Exit", self,
                shortcut=QtGui.QKeySequence.Redo, #------------------> Ovo ide u main window
                statusTip="Redo", triggered=self.exitCall)

        self.tool_bar.addAction(openAction)
        self.tool_bar.addAction(exitAction)


        # Create layouts to place inside widget
        self.controlLayout = QtWidgets.QHBoxLayout()
        self.controlLayout.setContentsMargins(0, 0, 0, 0)
        self.controlLayout.addWidget(self.playButton)
        self.controlLayout.addWidget(self.positionSlider)

        
        self.layout.addWidget(self.videoWidget)
        self.layout.addWidget(self.tool_bar)
        self.layout.addLayout(self.controlLayout)
        self.layout.addWidget(self.errorLabel)

        self.setLayout(self.layout)

        self.mediaPlayer.setVideoOutput(self.videoWidget)
        self.mediaPlayer.stateChanged.connect(self.mediaStateChanged)
        self.mediaPlayer.positionChanged.connect(self.positionChanged)
        self.mediaPlayer.durationChanged.connect(self.durationChanged)
        self.mediaPlayer.error.connect(self.handleError)
       

        self.path = None

        
        
        
        

       

    def openFile(self):
        fileName, _ = QFileDialog.getOpenFileName(self, "Open Movie",
                QtCore.QDir.homePath())

        if fileName != '':
            self.mediaPlayer.setMedia(
                    QMediaContent(QtCore.QUrl.fromLocalFile(fileName)))
            self.playButton.setEnabled(True)

    def exitCall(self):
        main.sys.exit(app.exec_())
        #sys.exit(app.exec_())# ----------> Ovo ide u main window

    def play(self):
        if self.mediaPlayer.state() == QMediaPlayer.PlayingState:
            self.mediaPlayer.pause()
        else:
            self.mediaPlayer.play()

    def mediaStateChanged(self, state):
        if self.mediaPlayer.state() == QMediaPlayer.PlayingState:
            self.playButton.setIcon(
                    self.style().standardIcon(QStyle.SP_MediaPause))
        else:
            self.playButton.setIcon(
                    self.style().standardIcon(QStyle.SP_MediaPlay))

    def positionChanged(self, position):
        self.positionSlider.setValue(position)

    def durationChanged(self, duration):
        self.positionSlider.setRange(0, duration)

    def setPosition(self, position):
        self.mediaPlayer.setPosition(position)

    def handleError(self):
        self.playButton.setEnabled(False)
        self.errorLabel.setText("Error: " + self.mediaPlayer.errorString())