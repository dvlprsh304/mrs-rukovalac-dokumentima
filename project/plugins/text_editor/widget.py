from PySide2 import QtWidgets, QtGui, QtCore, QtPrintSupport
from ui.structure_dock import StructureDock
from ui.tool_bar import ToolBar
from abc import ABC, abstractmethod
#from file_handler import FileHandler

class TextEdit(QtWidgets.QWidget):
    def __init__(self, parent):
        super().__init__(parent)
        self.main_window = parent
        # TODO: dodati meni

        self._layout = QtWidgets.QVBoxLayout()
        self.text_edit = QtWidgets.QTextEdit(self)
        self.text_edit.setAcceptRichText(False)
        
        #Dodavanje podesavanja za TextEditor
        fixedfont = QtGui.QFontDatabase.systemFont(QtGui.QFontDatabase.FixedFont)
        fixedfont.setPointSize(12)
        self.text_edit.setFont(fixedfont)
        
        #Kreiranje ToolBar-a
        self.tool_bar = QtWidgets.QToolBar("Naslov", self)

        #AllActions
        undoAction = QtWidgets.QAction(QtGui.QIcon("resources/icons/undo.png"),"&Undo", self,
                shortcut=QtGui.QKeySequence.Undo,
                statusTip="Undo", triggered=self.text_edit.undo)
                
        redoAction = QtWidgets.QAction(QtGui.QIcon("resources/icons/redo.png"),"&Redo", self,
                shortcut=QtGui.QKeySequence.Redo,
                statusTip="Redo", triggered=self.text_edit.redo)
       
        saveAction = QtWidgets.QAction(QtGui.QIcon("resources/icons/save.png"),"&Save", self,
                shortcut=QtGui.QKeySequence.Save,
                statusTip="Save", triggered=self.file_save)

        saveasAction = QtWidgets.QAction(QtGui.QIcon("resources/icons/saveas.png"),"&Save As", self,
                shortcut=QtGui.QKeySequence.SaveAs,
                statusTip="Save as", triggered=self.file_saveas)
        selectionAction = QtWidgets.QAction(QtGui.QIcon("resources/icons/selection.png"),"&Select all text", self,
                shortcut=QtGui.QKeySequence.SelectAll,
                statusTip="Undo", triggered=self.text_edit.selectAll)
        copyAction = QtWidgets.QAction(QtGui.QIcon("resources/icons/copy.png"),"&Copy", self,
                shortcut=QtGui.QKeySequence.Copy, #ctrl +c
                statusTip="Copy", triggered=self.text_edit.copy)
        
        cutAction = QtWidgets.QAction(QtGui.QIcon("resources/icons/cut.png"),"&Cut", self,
                shortcut=QtGui.QKeySequence.Cut,
                statusTip="Cut", triggered=self.text_edit.cut)
        pasteAction = QtWidgets.QAction(QtGui.QIcon("resources/icons/paste.png"),"&Paste", self,
                shortcut=QtGui.QKeySequence.Paste,
                statusTip="Paste", triggered=self.text_edit.paste)
        openAction = QtWidgets.QAction(QtGui.QIcon("resources/icons/openfile.png"),"&Open file", self,
                shortcut=QtGui.QKeySequence.Open,
                statusTip="Open file", triggered=self.file_open)
        
        self.tool_bar.addAction(saveAction)
        self.tool_bar.addAction(saveasAction)
        self.tool_bar.addAction(undoAction)
        self.tool_bar.addAction(redoAction)
        self.tool_bar.addAction(selectionAction)
        self.tool_bar.addAction(copyAction)
        self.tool_bar.addAction(cutAction)
        self.tool_bar.addAction(pasteAction)
        self.tool_bar.addAction(openAction)

        self._layout.addWidget(self.tool_bar)
        self._layout.addWidget(self.text_edit)

        self.setLayout(self._layout)
        self.path = None

    def file_open(self):
        path, _ = QtWidgets.QFileDialog.getOpenFileName(self, "Open file", "", "Text documents (*.txt);All files (*.*)")

        if path:
            try:
                with open(path, 'rU') as f:
                    text = f.read()

            except Exception as e:
                self.dialog_critical(str(e))

            else:
                self.path = path
                self.text_edit.setPlainText(text)
                self.update_title()
    def update_title(self):
        self.main_window.setWindowTitle("%s - Rukovalac dokumentima" % (os.path.basename(self.path) if self.path else "Untitled"))

    def file_save(self):
        if self.path is None:
            # Ako nemamo path, moramo da koristimo Save As.
            return self.file_saveas()
        
        self._save_to_path(self.path)

    def file_saveas(self):
        path, _ = QtWidgets.QFileDialog.getSaveFileName(self, "Save file", "", "Text documents (*.txt)")

        if not path:
            #If dialog is canceled, will return ''
            return

        self._save_to_path(path)

    def _save_to_path(self, path):
        text = self.text_edit.toPlainText()
        try:
            with open(path, 'w') as f:
                f.write(text)

        except Exception as e:
            self.dialog_critical(str(e))

        else:
            self.path = path