import sys
from PySide2 import QtWidgets, QtGui, QtCore
from plugin_framework.plugin_registry import PluginRegistry
from ui.main_window import MainWindow


if __name__ == "__main__":
    # TODO: aplikacija
    application = QtWidgets.QApplication(sys.argv)

    # TODO: main window
    main_window = MainWindow("Rukovalac dokumentima", QtGui.QIcon("resources/icons/rudok3.png"))
    #plugin_registry = PluginRegistry("plugins", main_window)
    
    #main_window.central_widget.setCurrentIndex(1)

    main_window.show()
    sys.exit(application.exec_())

