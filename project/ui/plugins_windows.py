from PySide2 import QtCore, QtWidgets, QtGui
from plugin_framework.plugin_registry import PluginRegistry

class PluginsWindow(QtWidgets.QWidget):
    def __init__(self, parent):
        super().__init__()
        self.main_window = parent
        
        self.setWindowTitle("Plugin Manager")
        self.setWindowIcon(QtGui.QIcon("resources/icons/plugin.png"))
        self.resize(300, 100)

        self.grid_layout = QtWidgets.QGridLayout()
        self.plugin_registry = PluginRegistry("plugins", self.main_window)
        #Prvi layout
        self.group_box = QtWidgets.QGroupBox("Dostupni pluginovi")

        self.radniprostor = QtWidgets.QRadioButton("Workspace")

        self.kolekcija = QtWidgets.QRadioButton("Collection")
        self.document = QtWidgets.QRadioButton("Document")
        self.content = QtWidgets.QRadioButton("Content")
        self.page = QtWidgets.QRadioButton("Page")
        self.slot = QtWidgets.QRadioButton("Slot")


        self.text_editor = QtWidgets.QRadioButton("Text editor")
        self.image_editor = QtWidgets.QRadioButton("Image editor")
        self.table_editor = QtWidgets.QRadioButton("Table editor")
        self.video_player = QtWidgets.QRadioButton("Video player")

        self.layout = QtWidgets.QVBoxLayout()

        self.layout.addWidget(self.radniprostor)

        self.layout.addWidget(self.kolekcija)
        self.layout.addWidget(self.document)
        self.layout.addWidget(self.content)
        self.layout.addWidget(self.page)
        self.layout.addWidget(self.slot)

        self.layout.addWidget(self.text_editor)
        self.layout.addWidget(self.image_editor)
        self.layout.addWidget(self.table_editor)
        self.layout.addWidget(self.video_player)
        self.group_box.setLayout(self.layout)
        
        self.radniprostor.clicked.connect(lambda: show_editor())

        self.kolekcija.clicked.connect(lambda: show_editor())
        self.document.clicked.connect(lambda: show_editor())
        self.content.clicked.connect(lambda: show_editor())
        self.page.clicked.connect(lambda: show_editor())
        self.slot.clicked.connect(lambda: show_editor())

        self.text_editor.clicked.connect(lambda: show_editor())
        self.image_editor.clicked.connect(lambda: show_editor())
        self.table_editor.clicked.connect(lambda: show_editor())
        self.video_player.clicked.connect(lambda: show_editor())

        #Drugi layout
        self.group_box2 = QtWidgets.QGroupBox("Aktivacija i deaktivacija")
        
        self.layout2 = QtWidgets.QVBoxLayout()
        self.button1 = QtWidgets.QPushButton("&Activate ")
        self.button1.clicked.connect(lambda: activated())
       
        self.button2 = QtWidgets.QPushButton("&Deactivate ")
        self.button2.clicked.connect(lambda: deactivated())
        self.layout2.addWidget(self.button1)
        self.layout2.addWidget(self.button2)
        self.group_box2.setLayout(self.layout2)
        self.group_box2.hide()
        
        self.message_box = QtWidgets.QMessageBox()
        self.message_box.setWindowTitle("Rukovalac dokumentima")

        def show_editor():
           self.group_box2.show()
        
        def activated():
            if self.text_editor.isChecked():
                print("text")
                self.plugin_registry.activate(555333)
                self.message_box.setText("Uspesno aktiviran "+"text" +" plugin")
                self.main_window.plugin1=True
            elif self.image_editor.isChecked():
                print("slika")
                self.plugin_registry.activate(2502)
                self.message_box.setText("Uspesno aktiviran " +"slika "+" plugin")
                self.main_window.plugin2=True     
            elif self.table_editor.isChecked():
                self.plugin_registry.activate(1312)   
                self.message_box.setText("Uspesno aktiviran " +"tabela"+" plugin")
                self.main_window.plugin3=True
                #self.main_window.central_widget.setCurrentWidget(self.main_window.table)
            elif self.video_player.isChecked():
                self.plugin_registry.activate(1008)   
                self.message_box.setText("Uspesno aktiviran " +"video"+" player")
                self.main_window.plugin4=True
            elif self.radniprostor.isChecked():
                self.plugin_registry.activate(1111)   
                self.message_box.setText("Uspesno aktiviran rad sa kolekcijama")
                self.main_window.plugin5=True
            elif self.kolekcija.isChecked():
                self.plugin_registry.activate(2222)   
                self.message_box.setText("Uspesno aktiviran rad sa kolekcijama")
                self.main_window.plugin6=True
            elif self.document.isChecked():
                self.plugin_registry.activate(3333)   
                self.message_box.setText("Uspesno aktiviran rad sa dokumentima")
                self.main_window.plugin6=True
            elif self.content.isChecked():
                self.plugin_registry.activate(4444)   
                self.message_box.setText("Uspesno aktiviran rad sa sadrzajem")
                self.main_window.plugin7=True
            elif self.page.isChecked():
                self.plugin_registry.activate(5555)   
                self.message_box.setText("Uspesno aktiviran rad sa stranicama")
                self.main_window.plugin8=True
            elif self.slot.isChecked():
                self.plugin_registry.activate(6666)   
                self.message_box.setText("Uspesno aktiviran rad sa slotovima")
                self.main_window.plugin9=True
            self.message_box.exec()
            
            
        
        def deactivated():
            if self.text_editor.isChecked(): #-----------------TEXT
                print("text")
                self.plugin_registry.deactivate(555333)
                self.message_box.setText("Uspesno deaktiviran "+"text" +" plugin")
                self.main_window.plugin3=False
                self.main_window.central_widget.setCurrentWidget(self.main_window.blank)
            elif self.image_editor.isChecked(): #-----------------IMAGE
                print("slika")
                self.plugin_registry.deactivate(2502)
                self.message_box.setText("Uspesno deaktiviran " +"slika "+" plugin")
                self.main_window.plugin3=False
                self.main_window.central_widget.setCurrentWidget(self.main_window.blank)
            elif self.table_editor.isChecked(): #-----------------TABLE
                self.plugin_registry.deactivate(1312)   
                self.message_box.setText("Uspesno deaktiviran " +"tabela"+" plugin")
                self.main_window.plugin3=False
                self.main_window.central_widget.setCurrentWidget(self.main_window.blank)
            elif self.video_player.isChecked(): #-----------------VIDEO
                self.plugin_registry.deactivate(1008)   
                self.message_box.setText("Uspesno deaktiviran " +"video"+" player")
                self.main_window.plugin4=False
                self.main_window.central_widget.setCurrentWidget(self.main_window.blank)
            elif self.radniprostor.isChecked(): #-----------------WORKSPACE
                self.plugin_registry.deactivate(1111)   
                self.message_box.setText("Uspesno deaktiviran rad sa kolekcijama")
                self.main_window.plugin5=False
            elif self.kolekcija.isChecked(): #-----------------KOLEKCIJA
                self.plugin_registry.deactivate(2222)   
                self.message_box.setText("Uspesno deaktiviran rad sa kolekcijama")
                self.main_window.plugin6=False
            elif self.document.isChecked(): #-----------------DOKUMENT
                self.plugin_registry.deactivate(3333)   
                self.message_box.setText("Uspesno deaktiviran rad sa dokumentima")
                self.main_window.plugin6=False
            elif self.content.isChecked(): #-----------------CONTENT
                self.plugin_registry.deactivate(4444)   
                self.message_box.setText("Uspesno deaktiviran rad sa sadrzajem")
                self.main_window.plugin7=False
            elif self.page.isChecked(): #-----------------PAGE
                self.plugin_registry.deactivate(5555)   
                self.message_box.setText("Uspesno deaktiviran rad sa stranicama")
                self.main_window.plugin8=False
            elif self.slot.isChecked(): #-----------------SLOT
                self.plugin_registry.deactivate(6666)   
                self.message_box.setText("Uspesno deaktiviran rad sa slotovima")
                self.main_window.plugin9=False
            self.message_box.exec()

       
        self.grid_layout.addWidget(self.group_box2)
        self.grid_layout.addWidget(self.group_box)
        

        self.setLayout(self.grid_layout)

