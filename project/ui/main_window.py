from PySide2 import QtCore, QtWidgets, QtGui, QtUiTools
from ui.structure_dock import StructureDock
from ui.menu_bar import MenuBar
from ui.tool_bar import ToolBar
from ui.about_window import AboutWindow
from ui.plugins_windows import PluginsWindow
import os
from plugins.text_editor.widget import TextEdit
from plugins.image_view.widget import ImageView
from plugins.table.widget import TablegeView
from plugins.video_player.widget import VideoPlayer
from plugins.collectionHandler.widget import CollectionHandler
from plugins.documentHandler.widget import DocumentHandler
from plugins.contentHandler.widget import ContentHandler
from plugins.slotHandler.widget import SlotHandler
from plugins.pageHandler.widget import PageHandler
from plugins.workspaceHandler.widget import WorkspaceHandler
from PySide2.QtUiTools import QUiLoader
import imp
import csv
import datetime
from PySide2.QtMultimedia import QMediaPlayer, QMediaPlaylist, QMediaContent
from ui.tree_view import TreeModel, TreeView
from ui import main_window


class MainWindow(QtWidgets.QMainWindow):
    def __init__(self, title, icon, parent=None):
        super().__init__(parent)
       
        self.setWindowTitle(title)
        self.setWindowIcon(icon)
        self.resize(1000, 720)
        
        #Pomocne varijable
        self.sat = 0
        self.brojac = 0 # Vezano za csv ucitavanje
        self.broj = 1 # Koristi se za ime workspace
        self.br = 0 # Koristi se za pronalazak workspacea u listi dokova
        #self.lista_dokova = [] # Za ucitavanje vise workspace

        #Inicijalizovanje widgeta
        
        self.imageview = ImageView(self)  
        self.texteditor = TextEdit(self)
        self.table = TablegeView(self)
        self.video = VideoPlayer(self)
        
        
        self.graphicsw = WorkspaceHandler(self)
        self.graphics = CollectionHandler(self)
        self.graphics1 = DocumentHandler(self)
        self.graphics2 = ContentHandler(self)
        self.graphics3 = PageHandler(self)
        self.graphics4 = SlotHandler(self)
        
        
        self.blank = QtWidgets.QWidget(self)

        self.menu_bar = MenuBar(self)
        self.tool_bar = ToolBar(self)
        self.tool_bar.setAutoFillBackground(True)
        self.status_bar = QtWidgets.QStatusBar(self)
        
        self.central_widget = QtWidgets.QStackedWidget(self) # STACKED WIDGET
        self.tab_widget = QtWidgets.QTabWidget(self.central_widget) # Tab Widget
        self.tab_widget.setTabsClosable(True)
        self.tab_widget.tabCloseRequested.connect(self.closeTab)

        self.structure_dock = StructureDock("Workspace 1", self)
    
        self.tree = self.structure_dock.tree
        self.structure_dock.tree.populate_data()
        #self.structure_dock2.tree.populate_data2()

        ''' Ovaj kod treba da bude u komandi da bi kreirao novi workspace

        self.structure_dock2 = StructureDock("Workspace 2", self)
        self.structure_dock2.tree.populate_data2()

        '''
       
        self.setMenuBar(self.menu_bar)
        
        self.addDockWidget(QtCore.Qt.LeftDockWidgetArea, self.structure_dock)
       
        self.setStatusBar(self.status_bar)
        #Setovanje central widgeta
        '''
        Posto je dodat tab widget ovaj deo se ne koristi

        #Dodavanje widgeta
        self.central_widget.addWidget(self.imageview)
        self.central_widget.addWidget(self.video)
        self.central_widget.addWidget(self.texteditor)
        self.central_widget.addWidget(self.table)
        self.central_widget.addWidget(self.graphics)
        self.central_widget.addWidget(self.graphics1)
        self.central_widget.addWidget(self.graphics2)
        self.central_widget.addWidget(self.graphics3)
        self.central_widget.addWidget(self.graphics4)
        self.central_widget.addWidget(self.graphicsw)
        #self.central_widget.addWidget(self.graphics_view)
        self.central_widget.addWidget(self.blank) #Blank je neophodan jer je on default prikaz bez aktiviranih pluginova
        self.central_widget.setCurrentWidget(self.blank) 
        '''
        self.addToolBar(self.tool_bar)
        
        self.central_widget.addWidget(self.tab_widget)        
        self.setCentralWidget(self.central_widget)


        #Provera aktiviranih pluginova
        #self.lista_dokova.append(self.structure_dock)
        self.status_bar.showMessage("Status bar")
        self.plugin1 = False
        self.plugin2 = False
        self.plugin3 = False
        self.plugin4 = False
        self.plugin5 = True
        self.plugin6 = True
        self.plugin7 = True
        self.plugin8 = True
        self.plugin9 = True
        self.plugin10 = True
        #self.plugin5 = False # KOLEKCIJE
        self.message_box = QtWidgets.QMessageBox()
        # Brojac
        
    def closeTab (self, currentIndex):
        currentQWidget = self.tab_widget.widget(currentIndex)
        self.tab_widget.removeTab(self.tab_widget.indexOf(currentQWidget))
    
    def save_asd(self):
        self.tree.save()

    def tool_bar_show_wp(self):
        self.central_widget.setCurrentWidget(self.graphicsw)
        self.graphicsw.lista.append("Workspace 1")
        #print(self.graphicsw.lista)

    def add_wp(self):
        self.broj += 1  # Koristi se za ime
        self.structure_dock2 = StructureDock("Workspace " + str(self.broj) , self)
        self.lista_dokova.append(self.structure_dock2)
        self.structure_dock2.tree.populate_data2()
        self.addDockWidget(QtCore.Qt.LeftDockWidgetArea, self.structure_dock2)
        self.graphicsw.lista.append("Workspace " + str(self.broj))
        self.graphicsw.populate()
        print(self.graphicsw.lista)
        self.lista_dokova[self.br + 1].setVisible(False) # Koristi za pristup listi dokova
        self.br += 1
        
    # Prikaz dokova na klik
    def switch(self, broj):
        self.brojic = int(broj) - 1 # Koristi se za pristup listi dokova
        for i in self.lista_dokova: # Sve dokove u listi hide-uje
            self.lista_dokova[self.lista_dokova.index(i)].setVisible(False)
        self.lista_dokova[self.brojic].setVisible(True) # Kliknuti dok prikaze

        


    #menuMethods
    def dialog_critical(self, s):
        dlg = QtWidgets.QMessageBox(self)
        dlg.setText(s)
        dlg.setIcon(QtWidgets.QMessageBox.Critical)
        dlg.show()

    def openFolderButton(self):
        path_to_folder = QtWidgets.QFileDialog.getExistingDirectory(self, self.tr("Open Folder"))
        print(path_to_folder)
        self.structure_dock.setRootPath(path_to_folder)   
    
    def update_title(self):
        MainWindow.setWindowTitle("%s - Rukovalac dokumentima" % (os.path.basename(self.path) if self.path else "Untitled"))
      
    def removeDockWidget(self):
        
        self.structure_dock.toggleViewAction()
        self.structure_dock.setVisible(False)

    def showDockWidget(self):
        self.structure_dock.toggleViewAction()
        self.structure_dock.setVisible(True)
    
    def showDialog(self):
        self.dialog = AboutWindow()
        self.dialog.show()

    def showPluginListDialog(self):
        self.dialog = PluginsWindow(self)
        self.dialog.show()


    def add_widget(self, widget):
        #Adds widget to central (stack) widget
        self.central_widget.addWidget(widget)
        
    
    def remove_widget(self, widget):
        self.central_widget.removeWidget(widget)  
        """
        self.actions_dict = {
            # FIXME: ispraviti ikonicu na X
            "quit": QtWidgets.QAction(QtGui.QIcon("resources/icons/document.png"), "&Quit", self)
            # TODO: dodati i ostale akcije za help i za npr. osnovno za dokument
            # dodati open...
        }
    
        # TODO: dodati metodu koja ce u fokus staviti trenutko aktivni plugin (njegov widget)``
        """

    def loadCsv(self, path): # Metoda za ucitavanje csv fajlova
        if self.brojac != 333:
            with open(path, "r") as fileInput:
                for self.row in csv.reader(fileInput):    
                    self.items = [
                        QtGui.QStandardItem(self.field)
                        for self.field in self.row
                    ]
                    self.table.model.appendRow(self.items)
                    self.brojac += 1
    '''
    def file_open(self):
        path, _ = QtWidgets.QFileDialog.getOpenFileName(self, "Open file", "", "Text documents (*.txt);All files (*.*)")
        if path.endswith((".txt")):
            if self.plugin1 == True:
                if path:
                    try:
                        with open(path, 'rU') as f:
                            text = f.read()

                    except Exception as e:
                        self.dialog_critical(str(e))
                    else:
                        self.path = path
                        self.texteditor.text_edit.setPlainText(text)
                self.central_widget.setCurrentWidget(self.texteditor)
            else:
                    self.message_box.setText("Text editor plugin nije aktiviran")
                    self.message_box.exec()
        if path.endswith((".png", ".jpg")):
            if self.plugin2 == True:
                self.imageview.set_image(path)
                self.central_widget.setCurrentWidget(self.imageview)
                self.update()
            else:
                self.message_box.setText("Image view plugin nije aktiviran")
                self.message_box.exec()
        if path.endswith((".csv")):
            if self.plugin3 == True:
                if self.brojac == 0:
                    self.loadCsv(file_name)
                    self.central_widget.setCurrentWidget(self.table)
                    self.update()
                else:
                    self.table.model.clear()
                    self.loadCsv(file_name)
                    self.central_widget.setCurrentWidget(self.table)
                    self.brojac == 0
                    self.update()
            else:
                self.message_box.setText("Table view plugin nije aktiviran")
                self.message_box.exec()
    '''

    '''
    def openNewFile(self, file_name):
        if file_name.endswith((".png", ".jpg")):
            if self.plugin2 == True:
                self.imageview.set_image(file_name)
                self.central_widget.setCurrentWidget(self.imageview)
                self.update()
            else:
                self.message_box.setText("Image view plugin nije aktiviran")
                self.message_box.exec()
        elif file_name.endswith((".txt")):
            if self.plugin1 == True:
                #self.imageview.set_image(file_name)
                with open(file_name, 'rU') as f:
                            text = f.read()
                self.texteditor.text_edit.setPlainText(text)
                self.central_widget.setCurrentWidget(self.texteditor)
                self.update()
            else:
                self.message_box.setText("Text editor plugin nije aktiviran")
                self.message_box.exec()
        elif file_name.endswith((".csv")):
            if self.plugin3 == True:
                if self.brojac == 0:
                    self.loadCsv(file_name)
                    self.central_widget.setCurrentWidget(self.table)
                    self.update()
                else:
                    self.table.model.clear()
                    self.loadCsv(file_name)
                    self.central_widget.setCurrentWidget(self.table)
                    self.brojac == 0
                    self.update()
            else:
                self.message_box.setText("Table view plugin nije aktiviran")
                self.message_box.exec()
        elif file_name.endswith(".avi"):
            if self.plugin4 == True:
                self.video.mediaPlayer.setMedia(
                        QMediaContent(QtCore.QUrl.fromLocalFile(file_name)))
                self.video.playButton.setEnabled(True)
                self.central_widget.setCurrentWidget(self.video)
            else:
                self.message_box.setText("Video player plugin nije aktiviran")
                self.message_box.exec()
        else:
            self.message_box.setText("Nepoznat tip fajla!")
            self.message_box.exec()
    '''
    def open_text(self, path):
        if self.plugin1 == True:
            #self.extension = self.file_path.split(".")[-1]
            with open(path, "rU") as f:
                text = f.read()
            self.texteditor.text_edit.setPlainText(text)
            self.tab_widget.addTab(self.texteditor, "Text")
            self.tab_widget.setCurrentWidget(self.texteditor)
            self.update()
        else:
            self.message_box.setText("Text editor plugin nije aktiviran")
            self.message_box.exec()

    def open_video(self, path):
        if self.plugin4 == True:
            self.video.mediaPlayer.setMedia(
                QMediaContent(QtCore.QUrl.fromLocalFile(path)))
            self.video.playButton.setEnabled(True)
            self.tab_widget.addTab(self.video, "Video")
            self.tab_widget.setCurrentWidget(self.video)
        else:
            self.message_box.setText("Video player plugin nije aktiviran")
            self.message_box.exec()

    def open_table(self, path):
        if self.plugin3 == True:
            if self.brojac == 0:
                self.loadCsv(path)
                self.tab_widget.addTab(self.table, "Tabela")
                self.tab_widget.setCurrentWidget(self.table)
                self.update()
            else:
                self.table.model.clear()
                self.loadCsv(path)
                self.tab_widget.addTab(self.table, "Tabela")
                self.tab_widget.setCurrentWidget(self.table)
                self.brojac == 0
                self.update()
        else:
            self.message_box.setText("Table view plugin nije aktiviran")
            self.message_box.exec()
    
    def open_image(self, path):
        if self.plugin2 == True:
            self.imageview.set_image(path)
            self.tab_widget.addTab(self.imageview, "Slika")
            self.tab_widget.setCurrentWidget(self.imageview)
            self.update()
        else:
            self.message_box.setText("Image view plugin nije aktiviran")
            self.message_box.exec()

 