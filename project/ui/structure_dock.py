from PySide2 import QtWidgets, QtGui, QtCore
from ui.tree_view import TreeView
from PySide2.QtMultimedia import QMediaPlayer, QMediaPlaylist, QMediaContent


class StructureDock(QtWidgets.QDockWidget):
    def __init__(self, title, parent):
        super().__init__(title, parent)
        self.main_window = parent
        self.message_box = QtWidgets.QMessageBox()
        
        self.tree = TreeView(self)
        
        self.brojac = 0
        self.br = 0
        self.lista_dokova = []
        #self.tree2 = TreeView2(self)
        self.tree.clicked.connect(self.item_clicked)
        self.tree.doubleClicked.connect(self.prikazi_kliknutog)
        self.setWidget(self.tree)

    def prikazi_kolekciju(self, index):
        indexx = index
        self.imee = self.tree.model.get_item(indexx) # Index od kliknutog pretvoren u item / kopirani.name -!
        
        if self.imee.title == "Collection": #Ako se klikne na kolekciju
            if self.main_window.plugin5 == True:
                if not self.main_window.graphics.lista: # Ako je lista scene tog kliknutog widgeta prazna nastavi
                    self.main_window.tab_widget.addTab(self.main_window.graphics,self.imee.name)
                    self.main_window.tab_widget.setCurrentWidget(self.main_window.graphics)
                    self.main_window.graphics.ime = self.imee # Saljemo ime kliknutog tom widgetu
                    self.main_window.graphics.indeks = indexx # Saljemo index kliknutog tom widgetu
                    for i in self.imee.elements:                    # Dodaj svako dete kliknutog u listu 
                            self.main_window.graphics.lista.append(i.name) #
                    self.main_window.graphics.populate() # Prikazi decu
                    self.main_window.graphics.populate2() # Prikazi naslov
                else: # Ako je neka scena vec otvorena obrisi i pocni ispocetka
                    #self.main_window.tab_widget.addTab(self.main_window.graphicss, self.imee.name)
                    self.main_window.graphics.clear()  #  
                    self.main_window.tab_widget.addTab(self.main_window.graphics,self.imee.name)
                    self.main_window.tab_widget.setCurrentWidget(self.main_window.graphics)
                    self.main_window.graphics.ime = self.imee #
                    self.main_window.graphics.indeks = indexx #
                    if not self.main_window.graphics.lista:
                        for i in self.imee.elements:                    
                            self.main_window.graphics.lista.append(i.name)
                    self.main_window.graphics.populate()
                    self.main_window.graphics.populate2()   
            else:
                self.message_box.setText("Rad sa kolekcijama plugin nije aktiviran")
                self.message_box.exec()

    def prikazi_dokument(self, index, ime):
        index = index
        self.ime = ime
        if self.ime.title == "Document":
            if self.main_window.plugin6 == True:
                if not self.main_window.graphics1.lista1:
                    self.main_window.tab_widget.addTab(self.main_window.graphics1,self.ime.name)
                    self.main_window.tab_widget.setCurrentWidget(self.main_window.graphics1)
                    self.main_window.graphics1.ime = self.ime
                    self.main_window.graphics1.indeks = index
                    for i in self.ime.elements:                    
                        self.main_window.graphics1.lista1.append(i.name)
                    self.main_window.graphics1.populate()
                    self.main_window.graphics1.populate2()
                else: # Ako je neka scena vec otvorena obrisi i pocni ispocetka
                    self.main_window.graphics1.clear()
                    self.main_window.tab_widget.addTab(self.main_window.graphics1,self.ime.name)
                    self.main_window.tab_widget.setCurrentWidget(self.main_window.graphics1)
                    self.main_window.graphics1.ime = self.ime
                    self.main_window.graphics1.indeks = index
                    
                    if not self.main_window.graphics1.lista1:
                        for i in self.ime.elements:                    
                            self.main_window.graphics1.lista1.append(i.name)
                    self.main_window.graphics1.populate()
                    self.main_window.graphics1.populate2()
            else:
                self.message_box.setText("Rad sa dokumentima plugin nije aktiviran")
                self.message_box.exec()

    def prikazi_content(self, index, ime):
        index = index
        self.ime = ime

        if self.ime.title == "Content":
            if self.main_window.plugin7 == True:
                if not self.main_window.graphics2.lista1:
                    self.main_window.tab_widget.addTab(self.main_window.graphics2,self.ime.name)
                    self.main_window.tab_widget.setCurrentWidget(self.main_window.graphics2)
                    self.main_window.graphics2.ime = self.ime
                    self.main_window.graphics2.indeks = index
                    for i in self.ime.elements:                    
                        self.main_window.graphics2.lista1.append(i.name)
                    self.main_window.graphics2.populate()
                    self.main_window.graphics2.populate2()
                else: # Ako je neka scena vec otvorena obrisi i pocni ispocetka
                    self.main_window.graphics2.clear()
                    self.main_window.tab_widget.addTab(self.main_window.graphics2,self.ime.name)
                    self.main_window.tab_widget.setCurrentWidget(self.main_window.graphics2)
                    self.main_window.graphics2.ime = self.ime
                    self.main_window.graphics2.indeks = index
                    
                    if not self.main_window.graphics2.lista1:
                        for i in self.ime.elements:                    
                            self.main_window.graphics2.lista1.append(i.name)
                    self.main_window.graphics2.populate()
                    self.main_window.graphics2.populate2()
            else:
                self.message_box.setText("Rad sa sadrzajima plugin nije aktiviran")
                self.message_box.exec()

    def prikazi_page(self, index, ime):
        index = index
        self.ime = ime
        if self.ime.title == "Page":
            if self.main_window.plugin8 == True:
                if not self.main_window.graphics3.lista1:
                    self.main_window.tab_widget.addTab(self.main_window.graphics3,self.ime.name)
                    self.main_window.tab_widget.setCurrentWidget(self.main_window.graphics3)
                    self.main_window.graphics3.ime = self.ime
                    self.main_window.graphics3.indeks = index
                    for i in self.ime.elements:                    
                        self.main_window.graphics3.lista1.append(i.name)
                    self.main_window.graphics3.populate()
                    self.main_window.graphics3.populate2()
                else: # Ako je neka scena vec otvorena obrisi i pocni ispocetka
                    self.main_window.graphics3.clear()
                    self.main_window.tab_widget.addTab(self.main_window.graphics3,self.ime.name)
                    self.main_window.tab_widget.setCurrentWidget(self.main_window.graphics3)
                    self.main_window.graphics3.ime = self.ime
                    self.main_window.graphics3.indeks = index
                    
                    if not self.main_window.graphics3.lista1:
                        for i in self.ime.elements:                    
                            self.main_window.graphics3.lista1.append(i.name)
                    self.main_window.graphics3.populate()
                    self.main_window.graphics3.populate2()
            else:
                self.message_box.setText("Rad sa sadrzajima plugin nije aktiviran")
                self.message_box.exec()

    def prikazi_slot(self, index, ime):
        index = index
        self.ime = ime
        if self.ime.title == "Slot":
            if self.main_window.plugin9 == True:
                if not self.main_window.graphics4.lista1:
                    self.main_window.tab_widget.addTab(self.main_window.graphics4,self.ime.name)
                    self.main_window.tab_widget.setCurrentWidget(self.main_window.graphics4)
                    self.main_window.graphics4.ime = self.ime
                    self.main_window.graphics4.indeks = index
                    for i in self.ime.elements:                    
                        self.main_window.graphics4.lista1.append(i.name)
                    self.main_window.graphics4.populate()
                    self.main_window.graphics4.populate2()
                else: # Ako je neka scena vec otvorena obrisi i pocni ispocetka
                    self.main_window.graphics4.clear()
                    self.main_window.tab_widget.addTab(self.main_window.graphics4,self.ime.name)
                    self.main_window.tab_widget.setCurrentWidget(self.main_window.graphics4)
                    self.main_window.graphics4.ime = self.ime
                    self.main_window.graphics4.indeks = index
                    
                    if not self.main_window.graphics4.lista1:
                        for i in self.ime.elements:                    
                            self.main_window.graphics4.lista1.append(i.name)
                    self.main_window.graphics4.populate()
                    self.main_window.graphics4.populate2()
            else:
                self.message_box.setText("Rad sa sadrzajima plugin nije aktiviran")
                self.message_box.exec()


    def prikazi_kliknutog(self): 
        self.tree.ram_index = self.tree.currentIndex()
        self.ime = self.tree.model.get_item(self.tree.ram_index)
        
        if self.ime.title == "Collection":
            self.prikazi_kolekciju(self.tree.ram_index)
        if self.ime.title == "Document":
            self.prikazi_dokument(self.tree.ram_index, self.ime)
        if self.ime.title == "Content":
            self.prikazi_content(self.tree.ram_index, self.ime)
        if self.ime.title == "Page":
            self.prikazi_page(self.tree.ram_index, self.ime)
        if self.ime.title == "Slot":
            self.prikazi_slot(self.tree.ram_index, self.ime)


    def item_clicked(self):
        index = self.tree.selectedIndexes()
        self.main_window.tool_bar.index = index
        selected_name = index[0].data()
        print(selected_name)
        all_items = self.tree.model.all_items
        for item in all_items:
            
            if selected_name == item.name and hasattr(item, 'file_path'):
                self.tree.file_path = item.file_path
                
                #Otvaranje tekstualnih fajlova
                if self.tree.file_path.endswith((".txt")):
                    self.main_window.open_text(self.tree.file_path)
                #Otvaranje video fajlova
                elif self.tree.file_path.endswith(".avi"):
                    self.main_window.open_video(self.tree.file_path)
                #Otvaranje tabela / .csv fajlova
                elif self.tree.file_path.endswith((".csv")):
                    self.main_window.open_table(self.tree.file_path)
                #Otvaranje slika / .png, .jpg fajlova
                elif self.tree.file_path.endswith((".png", ".jpg")):
                    self.main_window.open_image(self.tree.file_path)
                break
    
    

    