from PySide2 import QtWidgets, QtGui
from ui.structure_dock import StructureDock
from plugin_framework.plugin_registry import PluginRegistry
from plugins.text_editor.widget import TextEdit
#from model.tree_model import TreeModel
from model.workspace import Workspace
from ui.tree_view import TreeView, TreeModel

class MenuBar(QtWidgets.QMenuBar):
    def __init__(self, parent):
        super().__init__(parent)
        self.main_window = parent
       

        #PopulateMenu
        file_menu = QtWidgets.QMenu("File", self)
        edit_menu = QtWidgets.QMenu("Edit", self)
        view_menu = QtWidgets.QMenu("View", self)
        plugins_menu = QtWidgets.QMenu("Plugins", self)
        help_menu = QtWidgets.QMenu("Help", self)
        
        openFileAction = QtWidgets.QAction("&Open File", self,
                shortcut=QtGui.QKeySequence.Open,
                statusTip="Open File", triggered=self.dodaj)

        openFolderAction = QtWidgets.QAction("&Open Folder", self,
                shortcut=QtGui.QKeySequence.Open,
                statusTip="Open Folder", triggered=self.dodaj)
        #save
        saveAction = QtWidgets.QAction(QtGui.QIcon(':/images/save.png'),
                "&Save...", self, shortcut=QtGui.QKeySequence.Save,
                statusTip="Save the current form letter",
                triggered=self.save)
        #ViewSekcija
        sakrij = QtWidgets.QAction("&Sakrij workspace 1", self,
                statusTip="Remove Structure Dock", triggered=self.main_window.removeDockWidget)
        sakrij.setShortcut("Ctrl+D")

        prikazi = QtWidgets.QAction("&Prikazi workspace 1", self,
                statusTip="Show Structure Dock", triggered=self.main_window.showDockWidget)
        prikazi.setShortcut("Ctrl+D")

        sakrij2 = QtWidgets.QAction("&Sakrij workspace 2", self,
                statusTip="Remove Structure Dock", triggered=self.main_window.removeDockWidget)
        sakrij2.setShortcut("Ctrl+D")

        prikazi2 = QtWidgets.QAction("&Prikazi workspace 2", self,
                statusTip="Show Structure Dock", triggered=self.main_window.showDockWidget)
        prikazi2.setShortcut("Ctrl+D")
        
        zamena = QtWidgets.QAction("&Zameni", self,
                statusTip="Zameni", triggered=self.main_window.switch)
        zamena.setShortcut("Ctrl+D")


        #PluginsSekcija
        textEditorAction = QtWidgets.QAction("&TextEditor", self,
                statusTip="TextEditor")
        #HelpSekcija
        helpAction = QtWidgets.QAction("&Help", self,
                statusTip="Help")
        helpAction.setShortcut("Ctrl+H")
        #AboutSekcija
        aboutAction = QtWidgets.QAction("&About", self,
                statusTip="About", triggered=self.main_window.showDialog)
        aboutAction.setShortcut("Ctrl+A")
        #Plugin sekcija
        pluginListAction = QtWidgets.QAction("&Manager", self,
                statusTip="Manager", triggered=self.main_window.showPluginListDialog)
        pluginListAction.setShortcut("Ctrl+P")

        file_menu.addAction(openFileAction)
        file_menu.addAction(openFolderAction)
        file_menu.addAction(saveAction)

        help_menu.addAction(aboutAction)
        help_menu.addAction(helpAction)

        view_menu.addAction(sakrij)
        view_menu.addAction(prikazi)
        self.message_box = QtWidgets.QMessageBox()
        plugins_menu.addAction(pluginListAction)
        
       # toggle_structure_dock_action = self.main_window.structure_dock.toggleViewAction()
       # view_menu.addAction(toggle_structure_dock_action)
        
        
        
        #Edit sekcija
        insert = QtWidgets.QAction("&insert", self,
                statusTip="Insert", triggered=self.dodaj)

        addWorkspace = QtWidgets.QAction("&Dodaj workspace", self,
                statusTip="Dodaj workspace", triggered=self.main_window.add_wp)
        
        edit_menu.addAction(insert)
        edit_menu.addAction(addWorkspace)
        edit_menu.addAction(zamena)

        self.addMenu(file_menu)
        self.addMenu(edit_menu)
        self.addMenu(view_menu)
        self.addMenu(plugins_menu)
        self.addMenu(help_menu)

    def dodaj(self):
            pass
        
#### Dodato za save / ### Mozda izolovati kao dodatni plugin Save ?
    def save(self):
        filename, filtr = QtWidgets.QFileDialog.getSaveFileName(self,
                "Choose a file name")
        if not filename:
            return

        file = QtCore.QFile(filename)
        if not file.open(QtCore.QFile.WriteOnly | QtCore.QFile.Text):
            QtGui.QMessageBox.warning(self, "Dock Widgets",
                    "Cannot write file %s:\n%s." % (filename, file.errorString()))
            return
#### Dodato za save