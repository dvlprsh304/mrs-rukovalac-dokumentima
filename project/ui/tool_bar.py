from PySide2 import QtWidgets, QtGui
from ui.structure_dock import StructureDock
from ui.tree_view import TreeView
import json


class ToolBar(QtWidgets.QToolBar):
    def __init__(self, parent):
        super().__init__(parent)
        self.main_window = parent
        self.input_dialog = QtWidgets.QInputDialog()    
        

        self.index = None

        save = QtWidgets.QAction(QtGui.QIcon("resources/icons/save.png",),"&Save", self,
            shortcut=QtGui.QKeySequence.Undo,
            statusTip="Undo", triggered=self.main_window.save_asd)

        copy = QtWidgets.QAction(QtGui.QIcon("resources/icons/copy.png",),"&Copy", self,
            shortcut=QtGui.QKeySequence.Undo,
            statusTip="Copy", triggered=self.copy)

        paste = QtWidgets.QAction(QtGui.QIcon("resources/icons/paste.png",),"&Paste", self,
            shortcut=QtGui.QKeySequence.Undo,
            statusTip="Paste", triggered=self.paste)

        insert = QtWidgets.QAction(QtGui.QIcon("resources/icons/plus.png",),"&Insert", self,
                shortcut=QtGui.QKeySequence.Undo,
                statusTip="Insert", triggered=self.insert_command)

        delete = QtWidgets.QAction(QtGui.QIcon("resources/icons/delete.png"),"&Delete", self,
                shortcut=QtGui.QKeySequence.Undo,
                statusTip="Delete", triggered=self.delete_command)

        rename = QtWidgets.QAction(QtGui.QIcon("resources/icons/edit.png"),"&Rename", self,
                shortcut=QtGui.QKeySequence.Undo,
                statusTip="Rename", triggered=self.rename_command)
        
        workspace = QtWidgets.QAction(QtGui.QIcon("resources/icons/slot.png"),"&Workspace", self,
                shortcut=QtGui.QKeySequence.Undo,
                statusTip="Workspace", triggered=self.main_window.tool_bar_show_wp)


        #self.central_widget.setCurrentWidget(self.blank) 
        self.addAction(save)
        self.addAction(copy)
        self.addAction(paste)
        self.addAction(insert)
        self.addAction(delete)
        self.addAction(rename)
        self.addAction(workspace)

    #def show_workspace(self):
        

    def delete_command(self): # Brisanje na desni klik selektovanog elementa
        index = self.index[0]
        
        self.kliknuti = self.main_window.tree.model.get_item(index) 
        self.main_window.tree.model.beginRemoveRows(index.parent(), index.row(), index.row()) # Pocetak brisanja 
        self.main_window.tree.model.removeRow(index.row(), index.parent()) # Brisanje iz modela
        self.kliknuti.removeChildren(0, self.main_window.tree.model.rowCount(index)) # Brisanje dece iz item-a
        self.main_window.tree.model.endRemoveRows() # Zavrsetak brisanja redova
        #self.delete_lista(self.kliknuti.name) # Brisanje kliknutog elementa iz obe liste widget-a, radi uklanjanja elementa iz prikaza
        print(self.kliknuti.name)

    def copy(self):
        index = self.index[0]
        self.kopirani_indeks = self.index
        self.kopirani = self.main_window.tree.model.get_item(index)
        print("Kopiran: ", self.kopirani.name)

    def paste(self):
        index = self.index[0]

        

        self.main_window.tree.model.beginInsertRows(index, index.row(), index.column()) # Pocinjemo insertovanje

        self.kliknuti = self.main_window.tree.model.get_item(index) # Uzimamo item od selektovanog indexa
        self.dete_kliknutog = self.main_window.tree.model.create_child(self.kopirani.title, self.kliknuti, index) #Kreiramo dete kliknutog index
        self.dete_kliknutog.name = self.kopirani.name
        self.kliknuti.appendChild(self.dete_kliknutog) # Dodajemo dete 

        self.main_window.tree.model.endInsertRows() # Zavrsavamo insertovanje
        
        print("Paste-ovan: ", self.dete_kliknutog.name)

    def insert_command(self):
        # Insertovanje child-a kliknutog elementa
        index = self.index[0]
        self.main_window.tree.model.beginInsertRows(index, index.row(), index.column()) # Pocinjemo insertovanje
        self.kliknuti = self.main_window.tree.model.get_item(index) # Uzimamo item od selektovanog indexa
        
        self.dete_kliknutog = self.main_window.tree.model.create_child(self.kliknuti.child_title, self.kliknuti, index) #Kreiramo dete kliknutog index
        self.kliknuti.appendChild(self.dete_kliknutog) # Dodajemo dete 
        self.main_window.tree.model.endInsertRows() # Zavrsavamo insertovanje
        #self.main_window.prikazi_decu() # Posle insertovanja zelimo da posaljemo insertovan element odgovarajucem widgetu kako bi se prikazalo dodato dete

    def rename_command(self):
        index = self.index[0]
        self.kliknuti = self.main_window.tree.model.get_item(index) # Uzimamo item od selektovanog indexa 
        self.input = self.input_dialog.getText(self, 'Novo ime', 'Novo ime: ')  
        self.kliknuti.name = self.input[0]
    
    def save(self):
        index = self.index[0]
        self.kliknuti = self.main_window.tree.model.get_item(index) # Uzimamo item od selektovanog indexa
        self.naziv_datoteke = self.kliknuti.name
        self.string = self.kliknuti.to_dict() # Serijalizujemo kliknuti element
        out_file = open("meta/" + self.naziv_datoteke + ".json", "w")  # Otvaramo json dokument za pisanje
        json.dump(self.string, out_file, indent = 6)  #Zapisujemo serijalizovan dokument
        out_file.close()  # Zatvaramo json