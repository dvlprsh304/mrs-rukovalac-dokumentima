from PySide2 import QtWidgets, QtGui, QtCore
from model.tree_model import TreeModel
from model.workspace import Workspace
from model.collection import Collection
from model.document import Document
from model.content import Content
from model.page import Page
from model.slot import Slot
from model.video import Video
from model.text import Text
from model.table import Table
from model.image import Image
#from ui.menu_bar import MenuBar
from model.item import Item
import os
import json


class TreeView(QtWidgets.QTreeView):
    def __init__(self, parent):
        super().__init__(parent)
        self.main_window = parent.parentWidget()
        
        self.workspace = Workspace("Workspace")

        self.workspace2 = Workspace("Workspace 2")

        
        #self.setTreePosition(-1)

        #print(self.treePosition())
        '''
        self.data = None
        out_file = open("core.json", "w")
        json.dump("tree", out_file, indent = 6)  
        out_file.close()
        with open('core.json') as f:
            self.data = json.load(f)
            print(self.data)

        if self.data == "tree":
            self.populate_data()
            self.populate_data2()
            out_file = open("core.json", "w")
            json.dump("tree2", out_file, indent = 6)  
        '''
        
        self.file_path = None
        self.message_box = QtWidgets.QMessageBox()
        
        f = QtCore.QFile("ui\default.txt")
        f.open(QtCore.QIODevice.ReadOnly)
        f.close()
  
        self.kopirani = None
        self.kopirani_indeks = None
        self.save_kliknuti = None
        self.ram_index = None

        self.brojac_slike = 2
        self.brojac_teksta = 1
        self.brojac_videa = 1
        self.brojac_tabele = 1
        self.item = Item
        self.string = None
        self.menu = QtWidgets.QMenu('Menu', self)
        self.menu2 = QtWidgets.QMenu('Dodaj element', self) 
        self.menu3 = QtWidgets.QMenu("Insert", self) 
        self.menu4 = QtWidgets.QMenu("Tree action", self)
        self.menu5 = QtWidgets.QMenu("Under construction", self)

        self.menu.addAction(QtWidgets.QAction(QtGui.QIcon(""), 'Hide', self, triggered = self.hide))
        self.menu.addAction(QtWidgets.QAction(QtGui.QIcon(""), 'Save', self, triggered = self.save))
        self.menu5.addAction(QtWidgets.QAction(QtGui.QIcon(""), 'Load - ne radi', self, triggered = self.load))
        self.menu.addAction(QtWidgets.QAction(QtGui.QIcon(""), 'Copy', self, triggered = self.copy))
        self.menu.addAction(QtWidgets.QAction(QtGui.QIcon(""), 'Paste', self, triggered = self.paste))
        self.menu.addAction(QtWidgets.QAction(QtGui.QIcon(""), 'Rename', self, triggered = self.rename_command))
        self.menu.addAction(QtWidgets.QAction(QtGui.QIcon(""), 'Delete', self, triggered = self.delete_command))
        self.menu3.addAction(QtWidgets.QAction(QtGui.QIcon(""), 'Insert', self, triggered = self.insert_command))
        self.menu3.addAction(QtWidgets.QAction(QtGui.QIcon(""), 'Insert document', self, triggered = self.insert_document_command))
        self.menu3.addAction(QtWidgets.QAction(QtGui.QIcon(""), 'Insert collection', self, triggered = self.insert_collection_command))
        self.menu4.addAction(QtWidgets.QAction(QtGui.QIcon(""), 'Expand tree', self, triggered = self.expand))
        self.menu4.addAction(QtWidgets.QAction(QtGui.QIcon(""), 'Collapse tree', self, triggered = self.collapse))
        self.menu5.addAction(QtWidgets.QAction(QtGui.QIcon(""), 'Undo - ne radi', self))
        self.menu5.addAction(QtWidgets.QAction(QtGui.QIcon(""), 'Redo - ne radi', self))
        self.menu2.addAction(QtWidgets.QAction(QtGui.QIcon(""), 'Dodaj sliku', self,  triggered = self.dodaj_sliku))
        self.menu2.addAction(QtWidgets.QAction(QtGui.QIcon(""), 'Dodaj tekst', self,  triggered = self.dodaj_tekst))
        self.menu2.addAction(QtWidgets.QAction(QtGui.QIcon(""), 'Dodaj video', self,  triggered = self.dodaj_video))
        self.menu2.addAction(QtWidgets.QAction(QtGui.QIcon(""), 'Dodaj tabelu', self,  triggered = self.dodaj_tabelu))

        self.menu.addMenu(self.menu2)
        self.menu.addMenu(self.menu3)
        self.menu.addMenu(self.menu4)
        self.menu.addMenu(self.menu5)

        self.setSelectionMode(QtWidgets.QAbstractItemView.SingleSelection)
        #self.clicked.connect(self.item_clicked)
        
        #Drag and drop enabled
        self.setDragDropMode(QtWidgets.QAbstractItemView.InternalMove)
        self.setDragEnabled(True)
        self.setAcceptDrops(True)
        #self.setDropIndicator(True)
        self.setContextMenuPolicy(QtCore.Qt.CustomContextMenu)
        self.customContextMenuRequested.connect(self.open_context_menu)
        self.input_dialog = QtWidgets.QInputDialog()

    def hide(self):
        #self.setColumnHidden(0, True)
        #self.setRowHidden(self.model.workspace.row(), self.model.workspace.index(), True)
        index = self.currentIndex()
        self.setRowHidden(index.row(), index.parent(), True)


    def collapse(self): # Metoda za zatvaranje celog stabla
        self.collapseAll()

    def expand(self): # Metoda za otvaranje celog stabla
        self.expandAll()    
    
    def save(self):
        self.ram_index = self.currentIndex()
        self.kliknuti = self.model.get_item(self.ram_index) # Uzimamo item od selektovanog indexa
        self.save_kliknuti = self.kliknuti

        #self.lista = []
        self.lista_dece = [] # Deca kliknutog - DOKUMENTI
        self.lista_unuka = [] # - CONTENTI
        self.lista_praunuka = []
        self.lista_cukununuka = []
        self.lista_kurajberi = []

        self.save_in_list()
        self.save_in_file()
    
    def save_in_list(self):
        #for g in self.workspace.elements:
            #self.lista.append(g)
        for i in self.kliknuti.elements: # SVAKO DETE
            self.lista_dece.append(i) # DODAJ DETE
            for k in i.elements: # SVAKI UNUK
                self.lista_unuka.append(k) # DODAJ UNUKE
                for p in k.elements: # SVAKI PRAUNUK
                    self.lista_praunuka.append(p) # DODAJ PRAUNUKE
                    for o in p.elements: # SVAKI CUKUN UNUK
                        self.lista_cukununuka.append(o) # DODAJ CUKUN UNUKE
                        for z in o.elements: # SVAKI KURAJBER
                            self.lista_kurajberi.append(z) # DODAJ KURAJBERE

    def save_in_file(self):
        #self.naziv_datoteke = self.workspace.name
        #self.string = self.workspace.to_dict() # Serijalizujemo kliknuti element
        #out_file = open("meta/" + self.naziv_datoteke + ".json", "w")  # Otvaramo json dokument za pisanje
        #json.dump(self.string, out_file, indent = 6)  #Zapisujemo serijalizovan dokument
        #out_file.close()  # Zatvaramo json

        self.index = self.currentIndex()
        self.kliknuti = self.model.get_item(self.index)

        self.naziv_datoteke = self.kliknuti.name
        self.string = self.kliknuti.to_dict() # Serijalizujemo kliknuti element
        out_file = open("meta/" + self.naziv_datoteke + ".json", "w")  # Otvaramo json dokument za pisanje
        json.dump(self.string, out_file, indent = 6)  #Zapisujemo serijalizovan dokument
        out_file.close()  # Zatvaramo json


        #for g in self.lista:
            #out_file = open("meta/" + g.name + ".json", "w")  # Otvaramo json dokument za pisanje
            #json.dump(g.to_dict(), out_file, indent = 6)  #Zapisujemo serijalizovan dokument
            #out_file.close()  # Zatvaramo json
        for s in self.lista_dece:
            out_file = open("meta/"+  s.name + ".json", "w")  # Otvaramo json dokument za pisanje
            #out_file = open("meta/" + self.save_kliknuti.name + "/" + s.name + ".json", "w")  # Otvaramo json dokument za pisanje
            json.dump(s.to_dict(), out_file, indent = 6)  #Zapisujemo serijalizovan dokument
            for u in self.lista_unuka:
                out_file = open("meta/" + u.name + ".json", "w")  # Otvaramo json dokument za pisanje
                json.dump(u.to_dict(), out_file, indent = 6)  #Zapisujemo serijalizovan dokument
                for x in self.lista_praunuka:
                    self.naziv_datoteke = x.name
                    self.string = x.to_dict() # Serijalizujemo kliknuti element
                    out_file = open("meta/" + self.naziv_datoteke + ".json", "w")  # Otvaramo json dokument za pisanje
                    json.dump(self.string, out_file, indent = 6)  #Zapisujemo serijalizovan dokument
                    for t in self.lista_cukununuka:
                        self.naziv_datoteke =t.name
                        self.string = t.to_dict() # Serijalizujemo kliknuti element
                        out_file = open("meta/" + self.naziv_datoteke + ".json", "w")  # Otvaramo json dokument za pisanje
                        json.dump(self.string, out_file, indent = 6)  #Zapisujemo serijalizovan dokument
                        for i in self.lista_kurajberi:
                            self.naziv_datoteke = i.name
                            self.string = i.to_dict() # Serijalizujemo kliknuti element
                            out_file = open("meta/" + self.naziv_datoteke + ".json", "w")  # Otvaramo json dokument za pisanje
                            json.dump(self.string, out_file, indent = 6)  #Zapisujemo serijalizovan dokument
                            out_file.close()  # Zatvaramo json
                               
        

    def copy(self):
        index = self.currentIndex()
        self.kopirani_indeks = index
        self.kopirani = self.model.get_item(self.kopirani_indeks)
        
        print("Kopiran: ", self.kopirani.name)

    
    def paste(self):
        index = self.currentIndex() # Uzimamo index od selektovanog

        self.model.beginInsertRows(index, index.row(), index.column()) # Pocinjemo insertovanje

        self.kliknuti = self.model.get_item(index) # Uzimamo item od selektovanog indexa
        self.dete_kliknutog = self.model.create_child(self.kopirani.title, self.kliknuti, index) #Kreiramo dete kliknutog index
        self.dete_kliknutog.name = self.kopirani.name
        self.kliknuti.appendChild(self.dete_kliknutog) # Dodajemo dete 

        self.model.endInsertRows() # Zavrsavamo insertovanje
        
        print("Paste-ovan: ", self.dete_kliknutog.name)

        
    def load(self):
        # Ako nema index automatski pokazuje na workspace
        index = self.currentIndex()

        self.type = Collection #NAME
        self.ime = "1. kolekcija" #TITLE
        self.kliknuti = self.model.get_item(index)
        self.elements = ["1. dokument", # ELEMENTS
            "2. dokument",
            "3. dokument"]

        self.model.beginInsertRows(index, index.row(), index.column()) # Pocinjemo insertovanje
        self.dete_kliknutog = self.type(self.ime, self.kliknuti)
        self.kliknuti.appendChild(self.dete_kliknutog) # Dodajemo dete 
        self.model.endInsertRows() # Zavrsavamo insertovanje



        #self.model.beginInsertRows(index, index.row(), index.column()) # Pocinjemo insertovanje
        #self.kliknuti = self.model.get_item(index) # Uzimamo item od selektovanog indexa
        
        #self.dete_kliknutog = self.model.create_child(self.kliknuti.child_title, self.kliknuti, index) #Kreiramo dete kliknutog index
        #self.kliknuti.appendChild(self.dete_kliknutog) # Dodajemo dete 
        #self.model.endInsertRows() # Zavrsavamo insertovanje
        
    def dodaj_sliku(self):
        self.brojac_slike += 1
        self.brojac = str(self.brojac_slike)
        index = self.currentIndex() # Uzimamo index od selektovanog
        self.kliknuti = self.model.get_item(index) # Uzimamo item od selektovanog indexa
        if self.kliknuti.name == "Slot":
            self.model.beginInsertRows(index, index.row(), index.column()) 
            self.slika = Image(self.brojac + ". Image", self.slot)
            #self.slika.file_path = "ui\data\Slika3.jpg"
            path, _ = QtWidgets.QFileDialog.getOpenFileName(self, "Open image", "", "All files (*.*)")
            self.slika.file_path = path
            self.kliknuti.appendChild(self.slika) 
            self.model.endInsertRows() 
        else:
            self.message_box.setText("Slika se moze dodati samo u Slot!!!")
            self.message_box.exec()

    def dodaj_tekst(self):
        self.brojac_teksta += 1
        self.brojac = str(self.brojac_teksta)
        index = self.currentIndex() # Uzimamo index od selektovanog
        self.kliknuti = self.model.get_item(index) # Uzimamo item od selektovanog indexa
        if self.kliknuti.name == "Slot":
            self.model.beginInsertRows(index, index.row(), index.column()) 
            self.text = Text(self.brojac + ". Tekst", self.slot)
            #self.slika.file_path = "ui\data\Slika3.jpg"
            path, _ = QtWidgets.QFileDialog.getOpenFileName(self, "Open text", "", "All files (*.*)")
            self.text.file_path = path
            self.kliknuti.appendChild(self.text) 
            self.model.endInsertRows() 
        else:
            self.message_box.setText("Tekst se moze dodati samo u Slot!!!")
            self.message_box.exec()

    def dodaj_video(self):
        self.brojac_videa += 1
        self.brojac = str(self.brojac_videa)
        index = self.currentIndex() # Uzimamo index od selektovanog
        self.kliknuti = self.model.get_item(index) # Uzimamo item od selektovanog indexa
        if self.kliknuti.name == "Slot":
            self.model.beginInsertRows(index, index.row(), index.column()) 
            self.video = Video(self.brojac + ". Video", self.slot)
            #self.slika.file_path = "ui\data\Slika3.jpg"
            path, _ = QtWidgets.QFileDialog.getOpenFileName(self, "Open video", "", "All files (*.*)")
            self.video.file_path = path
            self.kliknuti.appendChild(self.video) 
            self.model.endInsertRows() 
        else:
            self.message_box.setText("Tekst se moze dodati samo u Slot!!!")
            self.message_box.exec()

    def dodaj_tabelu(self):
        self.brojac_tabele += 1
        self.brojac = str(self.brojac_tabele)
        index = self.currentIndex() # Uzimamo index od selektovanog
        self.kliknuti = self.model.get_item(index) # Uzimamo item od selektovanog indexa
        if self.kliknuti.name == "Slot":
            self.model.beginInsertRows(index, index.row(), index.column()) 
            self.table = Table(self.brojac + ". Table", self.slot)
            #self.slika.file_path = "ui\data\Slika3.jpg"
            path, _ = QtWidgets.QFileDialog.getOpenFileName(self, "Open table", "", "All files (*.*)")
            self.table.file_path = path
            self.kliknuti.appendChild(self.table) 
            self.model.endInsertRows() 
        else:
            self.message_box.setText("Tekst se moze dodati samo u Slot!!!")
            self.message_box.exec()

    def insert_command(self):
        # Insertovanje child-a kliknutog elementa
        index = self.currentIndex() # Uzimamo index od selektovanog
        self.model.beginInsertRows(index, index.row(), index.column()) # Pocinjemo insertovanje
        self.kliknuti = self.model.get_item(index) # Uzimamo item od selektovanog indexa
        
        self.dete_kliknutog = self.model.create_child(self.kliknuti.child_title, self.kliknuti, index) #Kreiramo dete kliknutog index
        self.kliknuti.appendChild(self.dete_kliknutog) # Dodajemo dete 
        self.model.endInsertRows() # Zavrsavamo insertovanje
        self.prikazi_decu() # Posle insertovanja zelimo da posaljemo insertovan element odgovarajucem widgetu kako bi se prikazalo dodato dete
        
    def prikazi_decu(self): # Funkcija se koristi za prosledjivanje child-ova widgetu u kojem je insertovano dete
        if self.kliknuti.title == "Collection":
            self.main_window.graphics.lista.append(self.dete_kliknutog.name)
            self.main_window.graphics.populate()    
        elif self.kliknuti.title == "Document":
            self.main_window.graphics1.lista1.append(self.dete_kliknutog.name)
            self.main_window.graphics1.populate()
        elif self.kliknuti.title == "Content":
            self.main_window.graphics2.lista1.append(self.dete_kliknutog.name)
            self.main_window.graphics2.populate()    
        elif self.kliknuti.title == "Page":
            self.main_window.graphics3.lista1.append(self.dete_kliknutog.name)
            self.main_window.graphics3.populate()   
        elif self.kliknuti.title == "Slot":
            self.main_window.graphics4.lista1.append(self.dete_kliknutog.name)
            self.main_window.graphics4.populate()    

    def insert_document_command(self): # Insertuje dokument u workspace
        self.documentx = Document("Document", self.workspace) # Kreiranje dokumenta
        self.workspace.appendChild(self.documentx) # Dodavanje dokumenta u vidu dodavanja child-a workspace-u
        self.collapseAll() # Alternativa za refresh

    def insert_collection_command(self): # Insertuje kolekciju direktno u workspace
        self.collectionx = Collection("Collection", self.workspace) # Kreiranje kolekcije
        self.workspace.appendChild(self.collectionx) # Dodavanje kolekcije u vidu dodavanja child-a workspace-u
        self.collapseAll() # Alternativa za refresh

    def rename_command(self):
        index = self.currentIndex() # Uzimamo index od selektovanog
        self.kliknuti = self.model.get_item(index) # Uzimamo item od selektovanog indexa 
        self.input = self.input_dialog.getText(self, 'Novo ime', 'Novo ime: ')  
        self.kliknuti.name = self.input[0]
        
    def delete_command(self): # Brisanje na desni klik selektovanog elementa
        cifra = self.selectedIndexes()
        index = cifra[0]
        #index = self.currentIndex() # Dobavljanje indexa selektovanog elementa
        self.kliknuti = self.model.get_item(index) # Dobavaljanje itema selektovanog indexa
        #print(self.kliknuti.child_title)
        self.model.beginRemoveRows(index.parent(), index.row(), index.row()) # Pocetak brisanja 
        self.model.removeRow(index.row(), index.parent()) # Brisanje iz modela
        self.kliknuti.removeChildren(0, self.model.rowCount(index)) # Brisanje dece iz item-a
        self.model.endRemoveRows() # Zavrsetak brisanja redova
        #self.delete_lista(self.kliknuti.name) # Brisanje kliknutog elementa iz obe liste widget-a, radi uklanjanja elementa iz prikaza
        print(self.kliknuti.name)


    def delete_command2(self, indexx):
        index = indexx
        self.kliknuti = self.model.get_item(index) # Dobavaljanje itema selektovanog indexa
        #print(self.kliknuti.child_title)
        self.model.beginRemoveRows(index.parent(), index.row(), index.row()) # Pocetak brisanja 
        self.model.removeRow(index.row(), index.parent()) # Brisanje iz modela
        self.kliknuti.removeChildren(0, self.model.rowCount(index)) # Brisanje dece iz item-a
        self.model.endRemoveRows() # Zavrsetak brisanja redova
        #self.delete_lista(self.kliknuti.name) # Brisanje kliknutog elementa iz obe liste widget-a, radi uklanjanja elementa iz prikaza
        print(self.kliknuti.name)

    def delete_lista(self, ime): # Brisanje kliknutog elementa iz obe liste widget-a, radi uklanjanja elementa iz prikaza
        #Uzimanje pozicije u listi oba elementa
        self.name = ime
        self.pozicija = self.main_window.graphics.lista.index(self.name)
        self.pozicija2 = self.main_window.graphics.lista_dodati.index(self.name)
        #Brisanje iz obe liste clanova koji su obrisani
        self.main_window.graphics.lista.pop(self.pozicija)
        self.main_window.graphics.lista_dodati.pop(self.pozicija2)
        
    def populate_data2(self):
        with open('meta/1. kolekcija.json') as f:
            self.data = json.load(f)
        print(self.data)

        self.name = self.data['name']
        self.title = self.data['title']
        

        self.model = TreeModel(self.workspace, self)
        self.setModel(self.model)

        '''
        self.collection1 = Collection("1. kolekcija", self.workspace)
        self.document1 = Document("1. dokument", self.collection1)
        self.document2 = Document("2. dokument", self.collection1)
        
        self.collection1.appendChild(self.document1)
        self.collection1.appendChild(self.document2)
        self.workspace.appendChild(self.collection1)
        self.workspace.appendChild(self.collection2)
        '''


    def populate_desni(self):
        self.model = TreeModel(self.workspace, self)
        self.setModel(self.model)




    '''
    def populatee_data(self):
        self.collection1 = Collection("1. kolekcija", self.workspace)



        for i in os.listdir("meta/"):
            with open("meta/" + i, 'r') as infile:
                obj = json.loads(infile.read())
                self.objekat = obj["title"] ( obj["name"], obj["parent"])
        
        

        #self.collection1 = Collection("1. kolekcija", self.workspace)
        self.workspace.appendChild(self.collection1)
        self.model = TreeModel(self.workspace, self)
        self.setModel(self.model)
    '''

    def populate_data(self): # Dodavanje elemenata u workspace
        #self.objekatt = Collection()
        """
        for i in os.listdir("meta/"):
            with open("meta/" + i, 'r') as infile:
                obj = json.loads(infile.read())
                if obj["name"].endswith("kolekcija"):
                    self.i = Collection(obj["name"], self.workspace)
                    self.workspace.appendChild(self.i)
        
        self.lista_dokumenta = []

        for i in os.listdir("meta/"): #
            with open("meta/" + i, 'r') as infile: #
                obj = json.loads(infile.read()) #
                if obj["name"].endswith("dokument"): # 
                        self.broj = int(obj["parent"][:1]) - 1
                        self.dokument = Document(obj["name"], self.workspace.elements[self.broj])
                        self.workspace.elements[self.broj].appendChild(self.dokument)
                        self.lista_dokumenta.append(self.dokument)

        self.lista_contenta = []

        for i in os.listdir("meta/"): #
            with open("meta/" + i, 'r') as infile: #
                obj = json.loads(infile.read()) #
                if obj["name"].endswith("content"): # 
                        self.broj = int(obj["parent"][:1]) - 1
                        self.content = Content(obj["name"], self.lista_dokumenta[self.broj])
                        self.lista_dokumenta[self.broj].appendChild(self.content)
                        self.lista_contenta.append(self.content)

        self.lista_page = []

        for i in os.listdir("meta/"): #
            with open("meta/" + i, 'r') as infile: #
                obj = json.loads(infile.read()) #
                if obj["name"].endswith("page"): # 
                        self.broj = int(obj["parent"][:1]) - 1
                        self.page = Page(obj["name"], self.lista_contenta[self.broj])
                        self.lista_contenta[self.broj].appendChild(self.page)
                        self.lista_page.append(self.page)

        self.lista_slota = []

        for i in os.listdir("meta/"): #
            with open("meta/" + i, 'r') as infile: #
                obj = json.loads(infile.read()) #
                if obj["name"].endswith("page"): # 
                        self.broj = int(obj["parent"][:1]) - 1
                        self.slot = Slot(obj["name"], self.lista_page[self.broj])
                        self.lista_page[self.broj].appendChild(self.slot)
                        self.lista_slota.append(self.slot)
        


        #print(self.workspace.elements[0].title)
        """

        
        
        #Collection
        self.collection1 = Collection("1. kolekcija", self.workspace)
        self.collection2 = Collection("2. kolekcija", self.workspace)
        self.collection3 = Collection("1. kolekcija", self.collection2)
        #Document
        self.document = Document("1. dokument", self.collection1)
        self.document2 = Document("2. dokument", self.collection1)
        self.document3 = Document("3. dokument", self.collection1)
        self.document4 = Document("1. dokument", self.workspace)
        #Content
        self.content = Content("1. content", self.document)
        #Page
        self.page = Page("Page", self.content)
        #self.page2 = Page("Page2", self.content)
        #self.page3 = Page("Page3", self.content)
        #Slot
        self.slot = Slot("Slot", self.page)
        #self.slot2 = Slot("Slot2", self.page2)
        #self.slot3 = Slot("Slot3", self.page3)
        #Text1
        self.text = Text("1. Text", self.slot)
        self.text.file_path = "ui\data\Tekst1.txt"
        #self.text.file_path = "ui\data\Tekst1.txt"
        #Text2
        #self.text2 = Text2("Text2", self.slot2)
        #self.text2.file_path = "ui\Tekst2.txt"
        #Text3
        #self.text3 = Text3("Text 3", self.slot3)
        #self.text3.file_path = "ui\Tekst3.txt"
        #Video1
        self.video = Video("1. Video", self.slot)
        self.video.file_path = "ui\data\Snimak1.avi"
        #self.video.file_path = "ui\data\Snimak1.avi"
        #Table
        self.table = Table("1. Table", self.slot)
        self.table.file_path = "ui\data\Tabela1.csv"
        #self.table.file_path = "ui\data\Tabela1.csv"
        #Slika
        self.image1 = Image("1. Image", self.slot)
        self.image1.file_path = "ui\data\Slika1.jpg"

        #self.image2 = Image("2. Image", self.slot)
        #self.image2.file_path = "ui\data\Slika2.jpg"
        #Lista
        self.lista = []
        self.lista.append(self.text)
        #self.lista.append(self.text2)
        #Slot
        self.slot.appendChild(self.video)
        self.slot.appendChild(self.text)
        #self.slot2.appendChild(self.text2)
       # self.slot3.appendChild(self.text3)
        self.slot.appendChild(self.table)
        self.slot.appendChild(self.image1)
        #self.slot.appendChild(self.image2)
        #Page
        self.page.appendChild(self.slot)
       # self.page2.appendChild(self.slot2)
      # self.page3.appendChild(self.slot3)
        #Content
        self.content.appendChild(self.page)
       # self.content.appendChild(self.page2)
      #  self.content.appendChild(self.page3)
        #Document
        self.document.appendChild(self.content)
        #Collection
        self.collection1.appendChild(self.document)
        self.collection1.appendChild(self.document2)
        self.collection1.appendChild(self.document3)
        self.collection2.appendChild(self.collection3)
        #Workspace
        self.workspace.appendChild(self.collection1)
        self.workspace.appendChild(self.collection2)
        self.workspace.appendChild(self.document4)
        
        
        #Model
        self.model = TreeModel(self.workspace, self)
        self.setModel(self.model)

    def open_context_menu(self): # Meni na desni klik
        self.menu.exec_(QtGui.QCursor.pos())

    def item_clicked(self):
        index = self.selectedIndexes()
        selected_name = index[0].data()
        all_items = self.model.all_items
        for item in all_items:
            if selected_name == item.name and hasattr(item, 'file_path'):
                self.file_path = item.file_path
                self.extension = self.file_path.split(".")[-1]
                print("ovo je", self.extension)
                break
    
    def show_command(self):
        pass

