from PySide2 import QtCore, QtWidgets, QtGui

class AboutWindow(QtWidgets.QWidget):
    def __init__(self):
        super().__init__()
        
        self.setWindowTitle("About")
        self.setWindowIcon(QtGui.QIcon("resources/icons/rudok3.png"))
        self.resize(300, 100)

        self.layout = QtWidgets.QVBoxLayout()
        self.naslov = QtWidgets.QLabel("<h1><b>Rukovalac Dokumentima</b></h1>")
        self.text = QtWidgets.QLabel("<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.<br> Lorem Ipsum has been the industry's standard dummy text ever since the 1500s,<br> when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>")
        self.verzija = QtWidgets.QLabel("1.0 (v1.0)")
        self.kopirajt = QtWidgets.QLabel("Copyright ©")
        self.label = QtWidgets.QLabel("All rights reserved.")
        self.central_widget = QtWidgets.QStackedWidget(self)

        self.slicica = QtWidgets.QLabel(self)
        pixmap = QtGui.QPixmap("resources/icons/aboutslika.png")
        self.slicica.setPixmap(pixmap)
        self.layout.addWidget(self.slicica)
        self.resize(pixmap.width(), pixmap.height())

        self.layout.addWidget(self.naslov)
        self.layout.addWidget(self.verzija)
        self.layout.addWidget(self.text)
        self.layout.addWidget(self.kopirajt)
        self.layout.addWidget(self.label)

        self.slicica.setAlignment(QtCore.Qt.AlignCenter)
        self.naslov.setAlignment(QtCore.Qt.AlignCenter)
        self.verzija.setAlignment(QtCore.Qt.AlignCenter)
        self.text.setAlignment(QtCore.Qt.AlignCenter)
        self.kopirajt.setAlignment(QtCore.Qt.AlignCenter)
        self.label.setAlignment(QtCore.Qt.AlignCenter)

        self.setLayout(self.layout)

        
        