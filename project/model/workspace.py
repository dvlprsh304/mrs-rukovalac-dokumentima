from PySide2 import QtWidgets, QtGui, QtCore
from model import *
from .item import Item
import json

class Workspace(Item): 
    item_title = "Workspace"
    child_title = "Collection", "Document"
    def __init__ (self, name):
        super(). __init__ (name)

    def toJson(self):
        return json.dumps(self, default=lambda o: o.__dict__)